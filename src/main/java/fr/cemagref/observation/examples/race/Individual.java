package fr.cemagref.observation.examples.race;

import fr.cemagref.observation.kernel.Observable;

public class Individual {
    
    // total individuals number
    private static int nb = 0;
    
    @Observable(description = "identifiant")
    private int id = ++nb;
    
    @Observable(description = "remaining distance")
    private double remainingDistance;
      
    /**
     * @param remainingDistance La distance initiale � parcourir par l'escargot
     */
    public Individual(double remainingDistance) {
        this.remainingDistance = remainingDistance;
    }
    
    public int getId() {
        return id;
    }

    @Observable(description = "remaining distance (method)")
    public double getRemainingDistance() {
        return remainingDistance;
    }

    public boolean step(double distance) {
        remainingDistance -= distance;
        if (remainingDistance <= 0) {
            remainingDistance = 0;
            return true;
        }
        return false;
    }
    
    public String toString() {
        return id+" - "+remainingDistance;
    }
}
