package fr.cemagref.observation.examples.race;

import fr.cemagref.observation.kernel.Observable;
import fr.cemagref.observation.kernel.ObservablesHandler;
import fr.cemagref.observation.kernel.ObservableManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Race {

    // current time

    private int time = 0;
    private List<Individual> individuals;
    @Observable(description = "Remaining distance")
    private double[] remainingDistance;
    private Random random = new Random(0);
    // length of race

    private int length;
    // constant handler

    private final ObservableManager observableManager;
    private final ObservablesHandler individualClassObservable;
    private final ObservablesHandler raceClassObservable;

    /**
     * @param nbIndividuals number of individuals
     * @param length la length de la course en centim�tres ;-)
     */
    public Race(ObservableManager observableManager, int nbIndividuals, int length) {
        this.observableManager = observableManager;
        individualClassObservable = observableManager.getObservable(Individual.class);
        raceClassObservable = observableManager.getObservable(this.getClass());
        individuals = new ArrayList<Individual>(nbIndividuals);
        remainingDistance = new double[nbIndividuals];
        this.length = length;
        for (int i = 0; i < nbIndividuals; i++) {
            Individual individual = new Individual(length);
            individuals.add(individual);
            if (individualClassObservable != null) {
                individualClassObservable.fireChanges(individual, time);
            }
        }
    }

    /**
     * <code>step</code> fait avancer chaque individuals d'une distance al�atoire comprise entre 0 et 1 cm
     */
    public boolean step() {
        try {
            Thread.currentThread().sleep(800);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        time++;
        boolean stop = false;
        for (Individual escargot : individuals) {
            if (escargot.step(random.nextDouble())) // the race stops when an individual is arrived
            {
                stop = true;
            }
            if (individualClassObservable != null) {
                individualClassObservable.fireChanges(escargot, time);
            }
            remainingDistance[individuals.indexOf(escargot)] = escargot.getRemainingDistance();
            raceClassObservable.fireChanges(this, time);
        }
        return stop;
    }
}
